using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ExtensionsLibrary.Tests
{
    public class JoinExtensionTest
    {
        [Fact]
        public void LeftOuterJoin_should_return_properOutput()
        {
            var leftList = new List<int>() { 1, 2, 3 };
            var rightList = new List<int>() { 1, 3, 4 };
            var leftJoinOutput = leftList.LeftOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.True(leftJoinOutput != null);
            Assert.True(leftJoinOutput.SingleOrDefault(x => x.left == 2 && x.right == 0) != null);
            Assert.True(leftJoinOutput.SingleOrDefault(x => x.left == 1 && x.right == 1) != null);
            Assert.True(leftJoinOutput.SingleOrDefault(x => x.left == 3 && x.right == 3) != null);
            Assert.True(leftJoinOutput.SingleOrDefault(x => x.right == 4) == null);
        }
        [Fact]
        public void LeftOuterJoin_When_LeftList_IsEmpty_ShouldReturn_EmptyList()
        {
            var leftList = new List<int>();
            var rightList = new List<int>() { 1, 3, 4 };
            var leftJoinOutput = leftList.LeftOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.True(leftJoinOutput != null && leftJoinOutput.Count() == 0);
        }
        [Fact]
        public void LeftOuterJoin_When_LeftList_IsNull_ShouldReturn_EmptyResult()
        {
            var leftList = new List<int>();
            leftList = null;
            var rightList = new List<int>() { 1, 3, 4 };
            var leftJoinOutput = leftList.LeftOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.Empty(leftJoinOutput);
        }

        [Fact]
        public void LeftOuterJoin_When_RightList_IsNull_ShouldReturn_AllRight_With_DefaultValues()
        {
            var leftList = new List<int>() { 1, 2, 3 };
            var rightList = new List<int>() { 1, 3, 4 };
            rightList = null;
            var leftJoinOutput = leftList.LeftOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.True(leftJoinOutput.Count() == leftList.Count());
            Assert.True(leftJoinOutput.Where(x => x.right > 0).ToList().Count() == 0);
        }
        [Fact]
        public void LeftOuterJoin_When_RightList_IsEmpty_ShouldReturn_AllRight_With_DefaultValues()
        {
            var leftList = new List<int>() { 1, 2, 3 };
            var rightList = new List<int>();
            var leftJoinOutput = leftList.LeftOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.True(leftJoinOutput.Count() == leftList.Count());
            Assert.True(leftJoinOutput.Where(x => x.right > 0).ToList().Count() == 0);
        }

        [Fact]
        public void RightOuterJoin_should_return_properOutput()
        {
            var leftList = new List<int>() { 1, 2, 3 };
            var rightList = new List<int>() { 1, 3, 4 };
            var leftJoinOutput = leftList.RightOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.True(leftJoinOutput != null);
            Assert.True(leftJoinOutput.SingleOrDefault(x => x.left == 0 && x.right == 4) != null);
            Assert.True(leftJoinOutput.SingleOrDefault(x => x.left == 1 && x.right == 1) != null);
            Assert.True(leftJoinOutput.SingleOrDefault(x => x.left == 3 && x.right == 3) != null);
            Assert.True(leftJoinOutput.SingleOrDefault(x => x.left == 2) == null);
        }

        [Fact]
        public void RightOuterJoin_When_RightList_IsEmpty_ShouldReturn_EmptyList()
        {
            var leftList = new List<int>() { 1, 3, 4 };
            var rightList = new List<int>();
            var rightJoinOutput = leftList.RightOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.True(rightJoinOutput != null && rightJoinOutput.Count() == 0);
        }
        [Fact]
        public void RightOuterJoin_When_RightList_IsNull_ShouldReturn_EmptyResult()
        {
            var leftList = new List<int>() { 1, 3, 4 };
            var rightList = new List<int>();
            rightList = null;
            var rightJoinOutput = leftList.RightOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.Empty(rightJoinOutput);
        }

        [Fact]
        public void RightOuterJoin_When_LeftList_IsEmpty_ShouldReturn_AllLeft_With_DefaultValues()
        {
            var leftList = new List<int>();
            var rightList = new List<int>() { 1, 3, 4 };
            var rightJoinOutput = leftList.RightOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.True(rightJoinOutput.Count() == rightList.Count());
            Assert.True(rightJoinOutput.Where(x => x.left > 0).ToList().Count() == 0);
        }
        [Fact]
        public void RightOuterJoin_When_LeftList_IsNull_ShouldReturn_AllRight_With_DefaultValues()
        {
            var leftList = new List<int>();
            leftList = null;
            var rightList = new List<int>() { 1, 2, 3 };
            var rightJoinOutput = leftList.RightOuterJoin(rightList, left => left, right => right, (left, right) => new { left = left, right = right }).ToList();
            Assert.True(rightJoinOutput.Count() == rightList.Count());
            Assert.True(rightJoinOutput.Where(x => x.left > 0).ToList().Count() == 0);
        }

    }
}
